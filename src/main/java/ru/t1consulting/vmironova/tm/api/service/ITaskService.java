package ru.t1consulting.vmironova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.vmironova.tm.enumerated.Status;
import ru.t1consulting.vmironova.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    @NotNull
    Task create(
            @Nullable String userId,
            @Nullable String name);

    @NotNull
    Task create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description);

    void changeTaskStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status);

    void changeTaskStatusByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable Status status);

    @NotNull
    List<Task> findAllByProjectId(
            @Nullable String userId,
            @Nullable String projectId);

    void updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description);

    void updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description);

}
