package ru.t1consulting.vmironova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.vmironova.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    void add(@Nullable AbstractCommand command);

    @Nullable
    AbstractCommand getCommandByArgument(@Nullable String argument);

    @Nullable
    AbstractCommand getCommandByName(@Nullable String command);

    @NotNull
    Collection<AbstractCommand> getTerminalCommands();

}
