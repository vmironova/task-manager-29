package ru.t1consulting.vmironova.tm.exception.field;

public class UserIdEmptyException extends AbstractFieldException {

    public UserIdEmptyException() {
        super("Error! User id is empty.");
    }

}
